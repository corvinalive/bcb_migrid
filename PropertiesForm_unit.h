//---------------------------------------------------------------------------

#ifndef PropertiesForm_unitH
#define PropertiesForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TPropertiesForm : public TForm
{
__published:	// IDE-managed Components
   TLabel *Label1;
   TLabel *DescriptionLabel;
   TLabel *Label2;
   TLabel *NameLabel;
   TLabel *Label4;
   TImage *NameImage;
   TImage *UnitImage;
   TLabel *UnitLabel;
   TLabel *Label3;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *KindLabel;
   TLabel *Label7;
   TLabel *StateLabel;
   TLabel *PointLabel;
   TLabel *MeasureLabel;
   TBitBtn *BitBtn1;
   TLabel *Label8;
   TLabel *Label9;
   TLabel *CalcLabel;
   TLabel *ShowLabel;
        TLabel *Label10;
        TLabel *ValueLabel;
private:	// User declarations
public:		// User declarations
   __fastcall TPropertiesForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
