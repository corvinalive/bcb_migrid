object SetAllForm: TSetAllForm
  Left = 313
  Top = 281
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
  ClientHeight = 180
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 2
    Width = 225
    Height = 32
    Caption = #1059#1089#1090#1072#1085#1072#1074#1083#1080#1074#1072#1077#1090' '#1079#1085#1072#1095#1077#1085#1080#1103' '#1074#1086' '#1074#1089#1077' '#1080#1079#1084#1077#1088#1077#1085#1080#1103' '#1090#1077#1082#1091#1097#1077#1081' '#1090#1086#1095#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 8
    Top = 42
    Width = 68
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1090#1086#1095#1082#1080':'
  end
  object Label3: TLabel
    Left = 8
    Top = 64
    Width = 150
    Height = 13
    Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1080#1079#1084#1077#1085#1103#1077#1084#1099#1093' '#1103#1095#1077#1077#1082
  end
  object Label4: TLabel
    Left = 8
    Top = 124
    Width = 146
    Height = 13
    Caption = #1059#1089#1090#1072#1085#1072#1074#1083#1080#1074#1072#1077#1084#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077':'
  end
  object Label5: TLabel
    Left = 170
    Top = 42
    Width = 32
    Height = 13
    Caption = 'Label5'
  end
  object Label6: TLabel
    Left = 20
    Top = 80
    Width = 285
    Height = 39
    AutoSize = False
    Caption = 'Label6'
    WordWrap = True
  end
  object BitBtn1: TBitBtn
    Left = 6
    Top = 152
    Width = 145
    Height = 25
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 158
    Top = 152
    Width = 145
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    Kind = bkCancel
  end
  object NumberEdit1: TNumberEdit
    Left = 170
    Top = 120
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    OnKeyDown = NumberEdit1KeyDown
  end
end
