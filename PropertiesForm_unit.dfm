object PropertiesForm: TPropertiesForm
  Left = 277
  Top = 143
  ActiveControl = BitBtn1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1074#1099#1073#1088#1072#1085#1085#1086#1081' '#1103#1095#1077#1081#1082#1077
  ClientHeight = 362
  ClientWidth = 441
  Color = clSkyBlue
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 4
    Top = 242
    Width = 65
    Height = 16
    Caption = #1054#1087#1080#1089#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DescriptionLabel: TLabel
    Left = 44
    Top = 261
    Width = 389
    Height = 63
    AutoSize = False
    Caption = 'Description Label'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 4
    Top = 34
    Width = 90
    Height = 16
    Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object NameLabel: TLabel
    Left = 175
    Top = 34
    Width = 71
    Height = 16
    Caption = 'NameLabel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 4
    Top = 58
    Width = 134
    Height = 16
    Caption = #1045#1076#1080#1085#1080#1094#1072' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
  end
  object NameImage: TImage
    Left = 175
    Top = 34
    Width = 105
    Height = 25
    Transparent = True
  end
  object UnitImage: TImage
    Left = 175
    Top = 58
    Width = 105
    Height = 25
    Transparent = True
  end
  object UnitLabel: TLabel
    Left = 175
    Top = 58
    Width = 57
    Height = 16
    Caption = 'UnitLabel'
  end
  object Label3: TLabel
    Left = 4
    Top = 82
    Width = 87
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1090#1086#1095#1082#1080':'
  end
  object Label5: TLabel
    Left = 4
    Top = 130
    Width = 121
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1080#1079#1084#1077#1088#1077#1085#1080#1103':'
  end
  object Label6: TLabel
    Left = 4
    Top = 106
    Width = 25
    Height = 16
    Caption = #1058#1080#1087
  end
  object KindLabel: TLabel
    Left = 175
    Top = 106
    Width = 60
    Height = 16
    Caption = 'KindLabel'
  end
  object Label7: TLabel
    Left = 4
    Top = 154
    Width = 70
    Height = 16
    Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
  end
  object StateLabel: TLabel
    Left = 175
    Top = 154
    Width = 266
    Height = 32
    AutoSize = False
    Caption = 'StateLabel 55555555555555 55 6666666666 6665'
    WordWrap = True
  end
  object PointLabel: TLabel
    Left = 175
    Top = 82
    Width = 64
    Height = 16
    Caption = 'PointLabel'
  end
  object MeasureLabel: TLabel
    Left = 175
    Top = 130
    Width = 87
    Height = 16
    Caption = 'MeasureLabel'
  end
  object Label8: TLabel
    Left = 4
    Top = 194
    Width = 157
    Height = 16
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1074#1099#1095#1080#1089#1083#1077#1085#1080#1103
  end
  object Label9: TLabel
    Left = 4
    Top = 218
    Width = 165
    Height = 16
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
  end
  object CalcLabel: TLabel
    Left = 175
    Top = 194
    Width = 61
    Height = 16
    Caption = 'CalcLabel'
  end
  object ShowLabel: TLabel
    Left = 175
    Top = 218
    Width = 67
    Height = 16
    Caption = 'ShowLabel'
  end
  object Label10: TLabel
    Left = 4
    Top = 8
    Width = 65
    Height = 16
    Caption = #1047#1085#1072#1095#1077#1085#1080#1077
  end
  object ValueLabel: TLabel
    Left = 175
    Top = 8
    Width = 69
    Height = 16
    Caption = 'ValueLabel'
  end
  object BitBtn1: TBitBtn
    Left = 2
    Top = 335
    Width = 437
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 0
    Kind = bkOK
  end
end
