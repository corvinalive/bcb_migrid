//---------------------------------------------------------------------------

#ifndef DM_unitH
#define DM_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TDM : public TDataModule
{
__published:	// IDE-managed Components
   TPopupMenu *MIGridPopup;
   TMenuItem *AddPoint;
   TMenuItem *DeletePoint;
   TMenuItem *N3;
   TMenuItem *AddMeasure;
   TMenuItem *DeleteMeasure;
   TMenuItem *N1;
   TMenuItem *PropertiesMenuItem;
private:	// User declarations
public:		// User declarations
   __fastcall TDM(TComponent* Owner);
   int CountUsed;
};
//---------------------------------------------------------------------------
extern PACKAGE TDM *DM;
//---------------------------------------------------------------------------
#endif
