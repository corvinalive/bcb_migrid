//---------------------------------------------------------------------------
#include <StdCtrls.hpp>
#pragma hdrstop

#include "Edit_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall TMIComboEdit::TMIComboEdit(TComponent* AOwner):TComboBox(AOwner)
{
   ParentCtl3D = false;
   Ctl3D = false;
   TabStop = false;
   DoubleBuffered = false;
   Style=csDropDownList;
}
//---------------------------------------------------------------------------
/*void __fastcall TMIComboEdit::WMGetDlgCode(TMessage& Msg)
{
   Msg.Result=DLGC_WANTARROWS;//DLGC_WANTCHARS
} */
//---------------------------------------------------------------------------
void __fastcall TMIComboEdit::Show(TRect* Rect)
{
   SetWindowPos(Handle, HWND_TOP, Rect->left, Rect->top, Rect->Width(), Rect->Height(), SWP_SHOWWINDOW/* | SWP_NOREDRAW*/);
   Visible=true;
   Apply=true;
   SetFocus();
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIComboEdit::WMKillFocus(TMessage& Msg)
{
   Msg.Result=0;
   Hide(Apply);
   Parent->Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIComboEdit::Hide(bool NeedApply)
{
   SetWindowPos(Handle,HWND_BOTTOM,Left,Top,Width,Height,SWP_HIDEWINDOW );

   if(FOnHideEdit)
      FOnHideEdit(this,NeedApply);
}
//---------------------------------------------------------------------------
void __fastcall TMIComboEdit::KeyDown(Word & Key, Classes::TShiftState Shift)
{
   TCustomComboBox::KeyDown(Key,Shift);
   switch (Key)
   {
     case VK_RETURN:
            Parent->SetFocus();
         break;
     case VK_ESCAPE:
         Apply=false;
         Parent->SetFocus();
        break;
   }

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TMIEdit::KeyPress(char &Key)
{
   bool Ok=false;
   if( (EditType==Int)&& (isdigit(Key)) )
      Ok=true;

   if(EditType==Float)
      {
      if(isdigit(Key)) Ok=true;
      if( (Key==',') || (Key=='.') )
         {
         Key=DecimalSeparator;
         Ok=true;
         }
      if( (Key=='e') || (Key=='�') || (Key=='�') )
         {
         Key='E';
         Ok=true;
         }
      }

   if(EditType==TextType) Ok=true;
   if(Key == VK_BACK) Ok=true;

   if(!Ok)  Key=0;
   TEdit::KeyPress(Key);
}
//---------------------------------------------------------------------------
__fastcall TMIEdit::TMIEdit(TComponent* AOwner):TEdit(AOwner)
{
   EditType=TextType;
   ParentCtl3D = false;
   Ctl3D = false;
   TabStop = false;
   BorderStyle = bsNone;                                           
   DoubleBuffered = false;
   Visible=false;
}
//---------------------------------------------------------------------------
void __fastcall TMIEdit::WMGetDlgCode(TMessage& Msg)
{
   Msg.Result=DLGC_WANTARROWS;                                 
}
//---------------------------------------------------------------------------
void __fastcall TMIEdit::Show(TRect* Rect)
{
   Left=Rect->left;
   Width=Rect->Width();
   Top=Rect->top;
   Height=Rect->Height();
//TODO: rem 29/06/06
   Visible=true;
   SetFocus();
//   SetWindowPos(Handle, HWND_TOP, Rect->left, Rect->top, Rect->Width(), Rect->Height(), SWP_SHOWWINDOW);

   SelfClose=false;
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIEdit::WMKillFocus(TMessage& Msg)
{
   if(!SelfClose)
      Hide(true);
}
//---------------------------------------------------------------------------
void __fastcall TMIEdit::Hide(bool NeedApply)
{
   if(Focused())
      {
      SelfClose=true;
      Parent->SetFocus();
      }

   Visible=false;
//   SetWindowPos(Handle,0,0,0,0,0,SWP_HIDEWINDOW	);
   if(FOnHideEdit)
      FOnHideEdit(this,NeedApply);
}
//---------------------------------------------------------------------------
void __fastcall TMIEdit::KeyDown(Word & Key, Classes::TShiftState Shift)
{
   switch (Key)
   {
     case VK_RETURN:
         Hide(true);
         return;
     case VK_ESCAPE:
         Hide(false);
        break;
   }
   TEdit::KeyDown(Key,Shift);
}
//---------------------------------------------------------------------------



