//---------------------------------------------------------------------------

#ifndef Edit_unitH
#define Edit_unitH


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *THideEditEvent)(System::TObject* Sender,
                                                      bool& Changed);
//---------------------------------------------------------------------------
class TMIComboEdit:public TComboBox
{
public:
   __fastcall TMIComboEdit(TComponent* AOwner);
//   void __fastcall WMGetDlgCode(TMessage& Msg);
   void __fastcall Show(TRect* Rect);
   void __fastcall WMKillFocus(TMessage& Msg);
   void __fastcall Hide(bool NeedApply);
   __property THideEditEvent OnHideEdit  = { read=FOnHideEdit, write=FOnHideEdit };
protected:
   DYNAMIC void __fastcall KeyDown(Word & Key, Classes::TShiftState Shift);
private:
   THideEditEvent FOnHideEdit;
   bool Apply;
__published:
   BEGIN_MESSAGE_MAP
//      VCL_MESSAGE_HANDLER(WM_GETDLGCODE, TMessage, WMGetDlgCode)
      VCL_MESSAGE_HANDLER(WM_KILLFOCUS, TMessage, WMKillFocus)
   END_MESSAGE_MAP(TComboBox)
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TMIEdit:public TEdit
{
public:
   __fastcall TMIEdit(TComponent* AOwner);
   void __fastcall WMGetDlgCode(TMessage& Msg);
   void __fastcall Show(TRect* Rect);
   void __fastcall WMKillFocus(TMessage& Msg);
   void __fastcall Hide(bool NeedApply);
   __property THideEditEvent OnHideEdit  = { read=FOnHideEdit, write=FOnHideEdit };
   enum TEditType {TextType=0, Int=1, Float=2} EditType;
protected:
   DYNAMIC void __fastcall KeyDown(Word & Key, Classes::TShiftState Shift);
   DYNAMIC void __fastcall KeyPress(char &Key);
private:
   THideEditEvent FOnHideEdit;
   bool SelfClose;
__published:
   BEGIN_MESSAGE_MAP
      VCL_MESSAGE_HANDLER(WM_GETDLGCODE, TMessage, WMGetDlgCode)
      VCL_MESSAGE_HANDLER(WM_KILLFOCUS, TMessage, WMKillFocus)
   END_MESSAGE_MAP(TEdit)
};
//---------------------------------------------------------------------------
#endif
