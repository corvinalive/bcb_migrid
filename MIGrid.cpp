//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "variable.h"
#include "PropertiesForm_unit.h"
#include "SetAllForm_unit.h"
#include "Edit_unit.h"
#include "MIGrid.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(TMIGrid *)
{
   new TMIGrid(NULL);
}
//---------------------------------------------------------------------------
__fastcall TMIGrid::TMIGrid(TComponent* Owner)
   : TCustomControl(Owner)
{
   FRows = new TList;
   FGridPopupMenu = CreateGridPopupMenu();
   FOffsetFromHeader=1;
   FPDO = new TMIPointDrawOptions;
   DoubleBuffered=true;
   FEditorMode=false;
/* 01/07/2006
   FEditor=CreateEditor();
   InsertControl(FEditor);
   FEditor->Visible=false;
   FEditor->OnHideEdit=OnHideEditor;
*/
   FComboEditor=CreateComboEditor();
   InsertControl(FComboEditor);
   FComboEditor->Visible=false;
   FComboEditor->OnHideEdit=OnHideEditor;

   FColor=clInfoBk;
   TabStop=true;
   Check[0]=new Graphics::TBitmap();
   Check[0]->LoadFromResourceName((int)HInstance, "Check01");
   Check[1]=new Graphics::TBitmap();
   Check[1]->LoadFromResourceName((int)HInstance, "Check02");
   Check[2]=new Graphics::TBitmap();
   Check[2]->LoadFromResourceName((int)HInstance, "Check03");
   FGridData=NULL;
   FRowHeight=18;
//   FFont = new TFont;
   Header = new THeaderControl(this);
   Header->OnSectionResize=OnResizeHeader;
   Header->OnDrawSection=OnDrawHeaderSection;
   Header->DoubleBuffered=true;
   InsertControl(Header);
   Header->Left=1;
   Header->Top=1;
   Header->Width=Width;
   Header->Height=24;
   Header->DragReorder=true;
   Header->OnSectionEndDrag=OnDragEnd;
   Header->OnSectionDrag=OnSectionDragEvent;
   LeftCol=0;
   FRowsHeight=0;
   FTopRow=0;
   Selection.RowIndex=-1;
//   SelectedRow=0;
   SelectedPoint=0;
   FPointRows.Rows=1;
   FPointRows.RowCells=1;
   FPointRows.LastRowCells=1;

   PropertiesForm = new TPropertiesForm(this);
   SetAllForm= new TSetAllForm(this);
}
//---------------------------------------------------------------------------
__fastcall TMIGrid::~TMIGrid()
{
   delete PropertiesForm;
   delete SetAllForm;
   int c=FRows->Count;
   for(int i=0;i<c;i++)
      delete (TMIRow*)FRows->Items[i];
   delete FRows;
   delete FPDO;
   delete Check[0];
   delete Check[1];
   delete Check[2];

   FGridData=NULL;
   //delete FFont;
   delete Header;
   if(FGridPopupMenu)   delete FGridPopupMenu;
}
//---------------------------------------------------------------------------
bool __fastcall TMIGrid::CanFocus(void)
{
   return true;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::CreateParams(TCreateParams &Params)
{
   TCustomControl::CreateParams(Params);
   Params.Style=Params.Style|WS_TABSTOP|WS_VSCROLL|WS_HSCROLL;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMSetFocus(TMessage &Msg)
{
   Msg.Result=0;
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMKillFocus(TWMKillFocus &Msg)
{
   if ((FEditor) && (Msg.FocusedWnd !=FEditor->Handle))
      FEditor->Hide(false);
   Msg.Result=0;
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::Click(void)
{
   SetFocus();
}
//---------------------------------------------------------------------------
namespace Migrid
{
   void __fastcall PACKAGE Register()
   {
       TComponentClass classes[1] = {__classid(TMIGrid)};
       RegisterComponents("Corvin", classes, 0);
   }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::SetColor(TColor value)
{
   if(FColor != value)
      {
      FColor = value;
      Invalidate();
      }
}
//---------------------------------------------------------------------------
/*void __fastcall TMIGrid::SetFont(TFont* value)
{
   if(FFont != value) FFont->Assign(value);
} */
//---------------------------------------------------------------------------
void __fastcall TMIGrid::SetRowHeight(int value)
{
   if(FRowHeight!=value)
      {
      FRowHeight=value;
      Invalidate();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::Setup()
{
   Header->Sections->Clear();
   int c=FGridData->MeasureColumns.Count;
   for(int i=0;i<c;i++)
      {//Add section to header
      Header->Sections->Add();
      Header->Sections->Items[i]->Alignment=taCenter;
      TVarOptions* vo=FGridData->MeasureVarOptions[GridData->MeasureColumns.Values[i]];
      Header->Sections->Items[i]->Text=vo->Name;
      Header->Sections->Items[i]->Width=vo->Width;

      if(vo->Bitmap!=-1)
         Header->Sections->Items[i]->Style=hsOwnerDraw;
      }
   AdjustHScrollBars();
   AdjustVScrollBars();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::Paint(void)
{
   Canvas->Brush->Color=FColor;
   Canvas->Rectangle(0,Header->Height,Width,Height);
   if(!FGridData) return;
   int PointCount=FGridData->Count;
   if(PointCount==0) return;
   int RowCount=FRows->Count;
   TMIRow* Row;
   Canvas->Font=Font;
   for(int i=FTopRow;i<RowCount;i++)
      DrawRow(i);
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::DrawMeasureRow(TVarContainer* m, int y1,bool Selected)
{
   int x=1;
   int cc=GridData->MeasureColumns.Count;
   for(int ii=FLeftCol;ii<cc;ii++)
      {//draw cell
      int index=GridData->MeasureColumns.Values[ii];
      TRect r;
      r.left=x-1;
      r.top=y1;
      int cellw=Header->Sections->Items[ii]->Width;
      r.right=r.left+cellw+1;
      r.Bottom=r.top+FRowHeight+1;
      bool sel=(Selected)&&(ii==Selection.ColumnsIndex);
      DrawCell(Canvas,r,m->Var[index],FGridData->MeasureVarOptions[index],sel);
      x+=cellw;
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::OnResizeHeader(THeaderControl* HeaderControl, THeaderSection* Section)
{
   AdjustHScrollBars();
   TVarOptions* vo;
   vo=FGridData->MeasureVarOptions[GridData->MeasureColumns.Values[Section->Index]];
   vo->Width=Section->Width;
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::DrawCell(TCanvas* Canvas,TRect Rect,TVar* v,TVarOptions* vo,bool Selected)
{
   int h=Canvas->TextHeight("A");
   AnsiString s;
   vo->FormatStr(v,s);
   if(vo->Type==TVarOptions::Bool)
      {
      int i;
      if(v->Value.b) i=1;
         else i=0;
      Canvas->Draw(Rect.left+(Rect.Width()-Check[i]->Width)/2,
                   Rect.top+(Rect.Height()-Check[i]->Height)/2,Check[i]);
      }
   else
      {
      int is=v->State;
      Canvas->Brush->Color=v->Colors[is*2+1];
      Canvas->Font->Color=v->Colors[is*2];
      int w=Canvas->TextWidth(s);
      Canvas->TextRect(Rect,Rect.left+(Rect.Width()-w)/2,Rect.top+(FRowHeight-h)/2,s);
      }
   if(Selected) Canvas->Brush->Color=clBlack;
   else Canvas->Brush->Color=clGray;
   Canvas->FrameRect(Rect);
//TODO: Changed 10/03/2006
   if(Selected && Focused())
      {
      TRect r(Rect);
      r.left+=1;
      r.top+=1;
      r.right-=1;
      r.Bottom-=1;
      Canvas->FrameRect(r);
      r.left+=1;
      r.top+=1;
      r.right-=1;
      r.Bottom-=1;
      Canvas->FrameRect(r);
      }
   Canvas->Brush->Color=FColor;
};
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMHScroll(Messages::TWMScroll & Msg)
{
   Msg.Result=0;
   switch (Msg.ScrollCode)
   {
   int c;
   int w,cc;
      case SB_LINERIGHT:
         LeftCol++;
         break;
      case SB_LINELEFT:
         LeftCol--;
         break;
      case SB_PAGELEFT:
         c=FLeftCol;
         w=Header->Sections->Items[c]->Width;
         while((w<ClientWidth)&&(c>0))
            {
            c--;
            w+=Header->Sections->Items[c]->Width;
            }
         LeftCol=c;
         break;
      case SB_PAGERIGHT:
         c=FLeftCol;
         cc=FGridData->MeasureColumns.Count-1;
         w=Header->Sections->Items[c]->Width;
         while((c<cc)&&(w<ClientWidth))
            {
            c++;
            w+=Header->Sections->Items[c]->Width;
            }
         LeftCol=c;
         break;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::AdjustHScrollBars(void)
{
      SetLeftCol(FLeftCol);
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::SetLeftCol(int lc)
{
   if(lc<0) lc=0;
   int c=Header->Sections->Count;
   if(lc>=c) return;
   //Calculate width from (0,0) grida to header's end
   int w=0;
   int p=0;//header's width before (0,0) grid
   FLeftCol=lc;
   for(int i=0;i<c;i++)
      {
      if(i==FLeftCol) p=w;
      w+=Header->Sections->Items[i]->Width;
      }
   Header->Align=alNone;
   Header->Width=Width+p;
   Header->Left=-p;
   SCROLLINFO s;
   s.cbSize=sizeof(s);
   s.nMin=0;
   if(LeftCol)s.nMax=Header->Width;
      else s.nMax=w;
   s.nPos=p;
   s.nPage=ClientWidth;
   s.fMask=SIF_RANGE|SIF_POS|SIF_PAGE;
   SetScrollInfo(Handle,SB_HORZ ,&s,true);
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::SetGridData(TGridData* G)
{
   FGridData = G;
   Setup();
   if(G->Count>0)
      {
      Selection.RowIndex=0;
      Selection.ColumnsIndex=0;
      SelectedRow = *((TMIRow*) FRows->Items[Selection.RowIndex]);
      SelectedPoint=FGridData->GridPoints[SelectedRow.PointIndex];
      }
   G->OnChangeItemsStructure=OnChangeItemsStructure;
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::KeyDown(Word &Key, Classes::TShiftState Shift)
{
   TCustomControl::KeyDown(Key,Shift);
   bool deleted=false;
   bool b;
   if(Selection.RowIndex==-1 && FRows->Count)
      {
      Selection.RowIndex = 0;
      Selection.ColumnsIndex=0;
      Invalidate();
      }
   switch (Key)
   {
     case VK_RIGHT:
         MoveRight();
         break;
     case VK_LEFT:
         MoveLeft();
         break;
     case VK_DOWN:
         MoveDown();
         break;
     case VK_UP:
         MoveUp();
         break;
     case VK_INSERT:
         TMICellData Data;
         b=GetCellData(Data);
         if(Data.Type==MeasureRow && b)
            {
            if(Data.PointIndex!=-1)
               {
               FGridData->AddNewMeasure(Data.PointIndex);
               UpdateRowsData();
               AdjustVScrollBars();
               FGridData->Calculate();
               Invalidate();
               }
            };
         if( (b && Data.Type==PointRow) || b==false)
            {//Point
            FGridData->AddNew();
            UpdateRowsData();
            AdjustVScrollBars();
            Selection.RowIndex = 0;
            Selection.ColumnsIndex=0;
            Invalidate();
            }
         break;

    case VK_DELETE:
         if(!GetCellData(Data))return;

         if(Data.Type==MeasureRow )
            {
            //delete Text
            TVarContainer* vc=FGridData->GridPoints[Data.PointIndex]->Measures[Data.MeasureIndex];
            int VarCount=vc->VarCount;
            TVar* Var;
            for(int i=0;i<VarCount;i++)
               {
               if(FGridData->MeasureVarOptions[i]->Type==TVarOptions::Text)
                  {
                  Var=vc->Var[i];
                  if(Var->Value.c!=NULL)
                     {
                     delete[] Var->Value.c;
                     Var->Value.c=0;
                     }
                  }
               }

            FGridData->GridPoints[Data.PointIndex]->Delete(Data.MeasureIndex);
            deleted=true;
            }
         else
            if(Data.PointIndex!=-1)
               {
               //delete Text
               TGridPoint* vc=FGridData->GridPoints[Data.PointIndex];
               int VarCount=vc->VarCount;
               TVar* Var;
               for(int i=0;i<VarCount;i++)
                  {
                  if(FGridData->PointVarOptions[i]->Type==TVarOptions::Text)
                     {
                     Var=vc->Var[i];
                     if(Var->Value.c!=NULL)
                        {
                        delete[] Var->Value.c;
                        Var->Value.c=0;
                        }
                     }
                  }
               //delete measures text
               TVarContainer* measure;
               int MeasuresCount=vc->Count;
               for(int i=0;i< MeasuresCount; i++)
                  {
                  measure=vc->Measures[i];
                  VarCount=measure->VarCount;
                  for(int i=0;i<VarCount;i++)
                     {
                     if(FGridData->MeasureVarOptions[i]->Type==TVarOptions::Text)
                        {
                        Var=measure->Var[i];
                        if(Var->Value.c!=NULL)
                           {
                           delete[] Var->Value.c;
                           Var->Value.c=0;
                           }
                        }
                     }
                  }//

               FGridData->Delete(Data.PointIndex);
               deleted=true;
               }
         if(deleted)
            {
            UpdateRowsData();
            if(Selection.RowIndex >= FRows->Count)
               Selection.RowIndex = FRows->Count-1;
            if(Selection.RowIndex==-1) break;
            SelectedRow =*( (TMIRow*) FRows->Items[Selection.RowIndex]);
            SelectedPoint=FGridData->GridPoints[SelectedRow.PointIndex];
            TMIRow* row=&SelectedRow;
            int lc;
            if(row->Type==MeasureRow)
               lc=FGridData->MeasureColumns.Count;
            else
               {
               if((row->RowIndex+1)<FPointRows.Rows)
                  lc=FPointRows.RowCells;
               else
                  lc=FPointRows.LastRowCells;
               }
            if((Selection.ColumnsIndex+1)>=lc)
               Selection.ColumnsIndex=lc-1;

            AdjustVScrollBars();
            Invalidate();
            }
         break;
     case VK_RETURN:
     case VK_F2:
         Edit();
         break;
     case VK_F3:
      SetAllMenuClick(0);
      break;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMGetDlgCode(Messages::TWMNoParams & Msg)
{
   Msg.Result=DLGC_WANTARROWS;
}
//---------------------------------------------------------------------------
int __fastcall TMIGrid::LastFullVisibleCol(int leftCol)
{
   int ColCount=Header->Sections->Count;
   if((!FGridData)||(ColCount==0))
      return 0;
   if(leftCol==-1)
      leftCol=FLeftCol;
   if((leftCol<0)||(leftCol>=ColCount))
      return 0;
   int x=0;
   int cw=ClientWidth;
   int col=leftCol;
   for(;col<ColCount;col++)
      {
      int w=Header->Sections->Items[col]->Width;
      if( (x+w)>cw)
         {
         col--;
         if(col>=0) return col;
            else return 0;
         }
      x+=w;
      }
   return ColCount;
}
//---------------------------------------------------------------------------
int __fastcall TMIGrid::DrawPointCell(int PointIndex, int ColumnIndex, int x,
                                                                int y, bool Sel)
{
   TGridPoint* p=FGridData->GridPoints[PointIndex];
   int VarIndex=FGridData->PointColumns.Values[ColumnIndex];
   TVarOptions* vo=FGridData->PointVarOptions[VarIndex];
   AnsiString caption=vo->Name;

   Canvas->Brush->Color=FPDO->CaptionColor;
   Canvas->FillRect(TRect(x,y,x+FPDO->CaptionWidth,y+FPDO->Height));

   TPoint ps[4];
   ps[0].x=x;//+FPDO->LineWidth;
   ps[0].y=y;//+FPDO->LineWidth;
   ps[1].x=x+FPDO->CaptionWidth+FPDO->ValueWidth;//-FPDO->LineWidth*2;
   ps[1].y=y;//+FPDO->LineWidth;
   ps[2].x=x+FPDO->CaptionWidth+FPDO->ValueWidth;//-FPDO->LineWidth*2;
   ps[2].y=y+FPDO->Height;//-2*FPDO->LineWidth;
   ps[3].x=x;//+FPDO->LineWidth;
   ps[3].y=y+FPDO->Height;//-2*FPDO->LineWidth;
   ps[4].x=x;//+FPDO->LineWidth;
   ps[4].y=y;//+FPDO->LineWidth;
   Canvas->Pen->Mode=pmCopy;
   Canvas->Pen->Style = psInsideFrame ;
   Canvas->Pen->Color=clBlack;
   Canvas->Pen->Width=FPDO->LineWidth;
   int curw=FPDO->LineWidth;
//TODO: Changed 10/03/2006
   if(Sel && Focused())
      {
      Canvas->Pen->Width=FPDO->SelectedLineWidth;
      curw=FPDO->SelectedLineWidth;
      }
   Canvas->Polyline(ps,4);

   int w;
   int h;
   //Draw caption
   if((vo->Bitmap==-1)||(!FGridData->OnGetBitmap))
      {
      w=Canvas->TextWidth(caption);
      h=Canvas->TextHeight("A");
      Canvas->Font->Color=clBlack;
      Canvas->TextOut(x+(FPDO->CaptionWidth-w)/2,y+(FPDO->Height-h)/2,vo->Name );
      }
   else
      {
      Graphics::TBitmap* b=FGridData->OnGetBitmap(vo->Bitmap);
      w=b->Width;
      h=b->Height;
      Canvas->CopyMode=cmSrcAnd;
      Canvas->Draw(x+(FPDO->CaptionWidth-w)/2,y+(FPDO->Height-h)/2,b);
      }

   //Draw variable value
   TVar* v=p->Var[VarIndex];
   AnsiString s;
   vo->FormatStr(v,s);
   if(vo->Type==TVarOptions::Bool)
      {
      int i;
      if(v->Value.b) i=1;
         else i=0;
      Canvas->Draw(x+FPDO->CaptionWidth+(FPDO->ValueWidth-Check[i]->Width)/2,
                   y+(FPDO->Height-Check[i]->Height)/2,Check[i]);
      }
   else
      {
      int is=v->State;
      Canvas->Brush->Color=v->Colors[is*2+1];
      Canvas->Font->Color=v->Colors[is*2];

      TRect r(x+FPDO->CaptionWidth+curw,y+curw,x+FPDO->CaptionWidth+FPDO->ValueWidth-2*curw,y+FPDO->Height-2*curw);
      int w=Canvas->TextWidth(s);
      int h=Canvas->TextHeight("A");
      Canvas->TextRect(r,r.left+(r.Width()-w)/2,r.top-curw+(FPDO->Height-h)/2,s);
      }
   Canvas->Pen->Width=1;
   return FPDO->CaptionWidth+FPDO->ValueWidth+FPDO->CellsSpace;
}
//---------------------------------------------------------------------------
TMIEdit* __fastcall TMIGrid::CreateEditor(void)
{
   return new TMIEdit(this);
}
//---------------------------------------------------------------------------
TMIComboEdit* __fastcall TMIGrid::CreateComboEditor(void)
{
   return new TMIComboEdit(this);
}
//---------------------------------------------------------------------------
bool __fastcall TMIGrid::GetCellRect(TRect& Rect, TMIGridCoord* Selection)
{
   if(!FGridData || FRows->Count==0)
      {
      Rect=TRect(0,0,0,0);
      return false;
      }
   TMIRow* Row=(TMIRow*) FRows->Items[Selection->RowIndex];
   TMIRow* topRow=(TMIRow*) FRows->Items[FTopRow];

   Rect.top = Header->Height+FOffsetFromHeader - topRow->YOffset + Row->YOffset;
   if (Row->Type==MeasureRow)
      {
      Rect.bottom=Rect.top+FRowHeight;
      Rect.left=0;
      for(int i=FLeftCol;i<Selection->ColumnsIndex;i++)
         Rect.left+=Header->Sections->Items[i]->Width;
      Rect.right=Rect.left+Header->Sections->Items[Selection->ColumnsIndex]->Width;
      }
   else
      {
      Rect.bottom=Rect.top+FPDO->Height;
      Rect.left=(FPDO->CaptionWidth+FPDO->ValueWidth+FPDO->CellsSpace)*Selection->ColumnsIndex;
      Rect.right=Rect.left+FPDO->CaptionWidth+FPDO->ValueWidth+FPDO->CellsSpace;
      }
   TRect temp;
   if(!IntersectRect(temp,Rect,ClientRect))
      {
      if( (Rect.right<0) ||(Rect.bottom<0) || (Rect.left>ClientWidth) || (Rect.top>ClientHeight) )
         return false;
      }
   return true;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMSize(TWMSize& Msg)
{
//Calculate FPointRows & FPointRowCells
  // int x=0;
   int dx;
   FPointRows.Rows=1;
   FPointRows.RowCells=0;
   if(!FGridData) return;
   int cp=FGridData->PointColumns.Count;
	dx=FPDO->CaptionWidth+FPDO->ValueWidth+FPDO->CellsSpace;
	FPointRows.RowCells=ClientWidth/dx;
	FPointRows.Rows=cp/FPointRows.RowCells;
	if(cp % FPointRows.RowCells) FPointRows.Rows++;

	FPointRows.LastRowCells=cp-(FPointRows.Rows-1)*FPointRows.RowCells;
   UpdateRowsData();
   AdjustVScrollBars();
   AdjustHScrollBars();
   Header->Width=Width;
   Msg.Result=0;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::Edit(TMIGridCoord* s,char Key)
{
   if(s==NULL) s=&Selection;
   if( (!FGridData) || (s->RowIndex==-1) || (s->ColumnsIndex==-1) )return;
   TRect r;
   if(!GetEditRect(r,s))
      return;
   EditedCell=Selection;
   TMICellData Data;
   GetCellData(Data,s);
   if(!FGridData->CanEdit(&Data)) return;
   if(Data.vo->Type==TVarOptions::Bool)
      {
      TVar::TValue val;
      val.b=!Data.v->Value.b;
      FGridData->SetValue(val,&Data);
      Invalidate();
      return;
      }
   if(Data.vo->Type==TVarOptions::List)
      {
      FComboEditor->Items->Clear();
      if(!Data.vo->Strings) return;
      FComboEditor->Items->Assign(Data.vo->Strings);
      FComboEditor->ItemIndex=Data.v->Value.i;
      FEditorMode=true;
      FComboEditor->Show(&r);
      return;
      }
   AnsiString s1;
   if(Key==0)
      {
      switch (Data.vo->Type)
         {
         case TVarOptions::Integer: s1=Data.v->Value.i;
            break;
         case TVarOptions::Float: s1=Data.v->Value.d;
            break;
         case TVarOptions::Text: s1=Data.v->Value.c;
            break;
         }
      }
   else
      s1=Key;

   if(FEditor!=0)
      {
      RemoveControl(FEditor);
      delete FEditor;
      FEditor=0;
      }


   FEditor=CreateEditor();
   InsertControl(FEditor);
//   FEditor->Visible=false;
   FEditor->OnHideEdit=OnHideEditor;

   FEditor->Text=s1;
   if(Key!=0)
      {
      FEditor->AutoSelect=false;
      FEditor->SelStart=s1.Length();
      }
   else
      FEditor->AutoSelect=true;
   FEditorMode=true;

   switch (Data.vo->Type)
      {
      case TVarOptions::Integer:
         FEditor->EditType=TMIEdit::Int;
         break;
      case TVarOptions::Float:
         FEditor->EditType=TMIEdit::Float;
         break;
      case TVarOptions::Text:
         FEditor->EditType=TMIEdit::TextType;
         break;
      }
   FEditor->Show(&r);
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::OnHideEditor(System::TObject* Sender,bool &NeedApply)
{
   FEditorMode=false;
   AnsiString s=FEditor->Text;
   if(NeedApply)
      {
      TVar::TValue val;
      TMICellData Data;
      GetCellData(Data,&EditedCell);
      try
         {
         switch (Data.vo->Type)
         {
           case TVarOptions::Integer: val.i=s.ToInt();
           break;
           case TVarOptions::Float: val.d=s.ToDouble();
           break;
           case TVarOptions::List: val.i=FComboEditor->ItemIndex;
           break;
           case TVarOptions::Text:
            {
            int i=s.Length();
            if(i)
               {
               val.c=new char[i+1];
               val.c[i]=0;
               strcpy(val.c,s.c_str());
               }
            else
               val.c=0;
            }
           break;
         }

         FGridData->SetValue(val,&Data);
         if( (Data.vo->Type==TVarOptions::Text) && (val.c) )
            delete[] val.c;
         Invalidate();
         }
      catch(EConvertError& err){MessageBeep(-1);};
      }
   if(FEditor!=0) FEditor->Visible=false;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MouseToCell(int x, int y, TMIGridCoord& Cell)
{
   Cell.RowIndex=-1;
   Cell.ColumnsIndex=-1;
   bool o=false;
   TMIGridCoord* pCell=&Cell;
   if(pCell==&Selection)
      {
      o=true;
//      SelectedRow=0;
      SelectedPoint=0;
      }
   if(!FGridData) return;
   int RowCount = FRows->Count;
   if(RowCount==0) return;
   TMIRow* toprow=(TMIRow*) FRows->Items[FTopRow];
   TMIRow* row;
   int offset=Header->Height + FOffsetFromHeader - toprow->YOffset;
   for(int i=FTopRow;i<RowCount;i++)
      {
      row=(TMIRow*) FRows->Items[i];
      int h;
      if(row->Type==MeasureRow)
         h=FRowHeight;
      else
         h=FPDO->Height;
      if( (y>(row->YOffset+offset)) && (y<(row->YOffset+offset+h)) )
         {
         Cell.RowIndex=i;
         break;
         }
      }
   if(Cell.RowIndex!=-1)
      {
      row=(TMIRow*) FRows->Items[Cell.RowIndex];
      //find column
      if(row->Type==MeasureRow)
         {//Find measure column
         int colcount=FGridData->MeasureColumns.Count;
         int cx=0;
         int w;
         for(int i=FLeftCol;i<colcount;i++)
            {
            w=Header->Sections->Items[i]->Width;
            if( (x>cx) && (x<(cx+w)) )
               {
               Cell.ColumnsIndex=i;
               break;
               }
            cx+=w;
            }
         }
      else
         {//find cell in point rows
         int lc;
         if((row->RowIndex+1)<FPointRows.Rows)
            lc=FPointRows.RowCells;
         else
            lc=FPointRows.LastRowCells;
         int cx=0;
         int w=FPDO->CaptionWidth+FPDO->ValueWidth+FPDO->CellsSpace;
         for(int i=0;i<lc;i++)
            {
            if( (x>cx) && (x<(cx+w)) )
               {
               Cell.ColumnsIndex=i;
               break;
               }
            cx+=w;
            }
         }
      }
   if( (o) && (Selection.RowIndex!=-1)&&(Selection.ColumnsIndex!=-1))
      {
      SelectedRow = *((TMIRow*) FRows->Items[Selection.RowIndex]);
      SelectedPoint=FGridData->GridPoints[SelectedRow.PointIndex];
      }
   return;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y)
{
   TCustomControl::MouseDown(Button,Shift,X,Y);
   TMIGridCoord c;
   MouseToCell(X,Y,c);
   if((c.RowIndex!=-1)&&(c.ColumnsIndex!=-1))
      {
      Selection=c;
      SelectedRow = *((TMIRow*) FRows->Items[Selection.RowIndex]);
      SelectedPoint=FGridData->GridPoints[SelectedRow.PointIndex];
      }
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMLButtonDblClick(TWMLButtonDblClk& Msg)
{
   MouseToCell(Msg.XPos,Msg.YPos,Selection);
   Edit();
   Msg.Result=0;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::KeyPress(char &Key)
{
   if(FEditorMode) return;
   if(  !( ((Key>='0') && (Key<='9')) || ((Key=='.') || (Key==',')) )) return;
   if((Key=='.') || (Key==','))
      Key=DecimalSeparator;
   Edit(0,Key);
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MoveRight()
{
   if( FRows->Count==0) return;
   TMIRow* row = (TMIRow*) FRows->Items[Selection.RowIndex];
   if(row->Type==PointRow)
      {
      int lastcol;
      if((row->RowIndex+1)==FPointRows.Rows)
         lastcol=FPointRows.LastRowCells;
      else
         lastcol=FPointRows.RowCells;
      if((Selection.ColumnsIndex+1)<lastcol)
         {
         Selection.ColumnsIndex++;
         Invalidate();
         }
      }
   else
      {
      if((Selection.ColumnsIndex+1)<FGridData->MeasureColumns.Count)
         {
         Selection.ColumnsIndex++;
         int lvc=LastFullVisibleCol();
         if(Selection.ColumnsIndex>lvc)
            {
            int leftcol=FLeftCol;
            do
               {
               leftcol++;
               lvc=LastFullVisibleCol(leftcol);
               }
            while(Selection.ColumnsIndex>lvc);
            LeftCol=leftcol;
            }
         else
            Invalidate();
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MoveLeft()
{
   if( FRows->Count==0) return;
   if(Selection.ColumnsIndex)
      {
      Selection.ColumnsIndex--;
      TMIRow* row = (TMIRow*) FRows->Items[Selection.RowIndex];
      if((row->Type==MeasureRow)&&(Selection.ColumnsIndex<FLeftCol))
         LeftCol=Selection.ColumnsIndex;
      else
         Invalidate();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MoveDown()
{
   if( (Selection.RowIndex+1)<FRows->Count)
      {
      Selection.RowIndex++;

      TMIRow PSR=SelectedRow;

      SelectedRow =*( (TMIRow*) FRows->Items[Selection.RowIndex]);
      SelectedPoint=FGridData->GridPoints[SelectedRow.PointIndex];

      TMIRow* row=&SelectedRow;
      if(row->Type != PSR.Type)
         {
         TRect r;
         Selection.RowIndex--;
         GetCellRect(r,&Selection);
         Selection.RowIndex++;
         TMIGridCoord cc;
         MouseToCell(r.left+r.Width()/2,r.bottom+4, cc);
         if(cc.ColumnsIndex!=-1)
            Selection=cc;
         }

      int lc;
      if(row->Type==MeasureRow)
         lc=FGridData->MeasureColumns.Count;
      else
         {
         if((row->RowIndex+1)<FPointRows.Rows)
            lc=FPointRows.RowCells;
         else
            lc=FPointRows.LastRowCells;
         }
      if((Selection.ColumnsIndex+1)>=lc)
         Selection.ColumnsIndex=lc-1;

      if(Selection.RowIndex>LastVisibleRow())
         {
         int toprow=FTopRow;
         do
            {
            toprow++;
            }
         while(Selection.RowIndex>LastVisibleRow(toprow));
         TopRow=toprow;
         }
      else
         Invalidate();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MoveUp()
{
   if( (Selection.RowIndex!=-1)&&(Selection.RowIndex))
      {
      Selection.RowIndex--;
      TMIRow PSR=SelectedRow;

      SelectedRow = *((TMIRow*) FRows->Items[Selection.RowIndex]);
      SelectedPoint=FGridData->GridPoints[SelectedRow.PointIndex];

      TMIRow* row=&SelectedRow;
      if(row->Type != PSR.Type)
         {
         TRect r;
         Selection.RowIndex++;
         GetCellRect(r,&Selection);
         Selection.RowIndex--;
         TMIGridCoord cc;
         MouseToCell(r.left+r.Width()/2,r.top-4, cc);
         if(cc.ColumnsIndex!=-1)
            Selection=cc;
         }

      int lc;
      if(row->Type==MeasureRow)
         lc=FGridData->MeasureColumns.Count;
      else
         {
         if((row->RowIndex+1)<FPointRows.Rows)
            lc=FPointRows.RowCells;
         else
            lc=FPointRows.LastRowCells;
         }
      if((Selection.ColumnsIndex+1)>=lc)
         Selection.ColumnsIndex=lc-1;

      if(FTopRow>Selection.RowIndex)
         TopRow=Selection.RowIndex;
      else
         Invalidate();
      }
}
//---------------------------------------------------------------------------
bool __fastcall TMIGrid::GetEditRect(TRect& Rect, TMIGridCoord* Selection)
{
   bool r=GetCellRect(Rect,Selection);
   if(r)
      {
      if(SelectedRow.Type==PointRow)
         Rect.left+=FPDO->CaptionWidth;
      }
   return r;
}
//---------------------------------------------------------------------------
Menus::TPopupMenu* __fastcall TMIGrid::GetPopupMenu(void)
{
   if(FEditorMode)
      return NULL;
   //Clear older items;
   TMenuItem* Items=FGridPopupMenu->Items;
   int c=Items->Count;
   int ii=0;
   for(int i=0;i<c;i++)
      {
      if(Items->Items[ii]->Tag!=-1)
         Items->Delete(ii);
      else
         ii++;
      }
   TMICellData Data;
   if(!GetCellData(Data)) return 0;
   //������������� ���� "���������� ���", ���� ��� �����, ������ �������������,
   //��� ������ �����, ��� ������ ����� ��� ������
   if( (Data.Type==PointRow) ||
       (Data.vo->Kind!=TVarOptions::InputData) ||
       (
         !((Data.vo->Type==TVarOptions::Integer)||(Data.vo->Type==TVarOptions::Float))
        )
      )
      SetAllMenuItem->Enabled=false;
   else
      SetAllMenuItem->Enabled=true;
   FGridPopupMenu->OwnerDraw=true;
   FGridData->ModifyPopupMenu(FGridPopupMenu,&Data);
   return FGridPopupMenu;
}
//---------------------------------------------------------------------------
TPopupMenu* __fastcall TMIGrid::CreateGridPopupMenu()
{
   TPopupMenu* m = new TPopupMenu(this);
   TMenuItem* mi = new TMenuItem(m);
   mi->Caption="&�������� �����";
   mi->Name="AddPoint";
   mi->Tag=-1;
   mi->OnDrawItem=MenuDrawItem;
   mi->OnMeasureItem=MenuMeasureItem;
   m->Items->Add(mi);
   mi->OnClick=AddPointMenuClick;
   mi = new TMenuItem(m);
   mi->Tag=-1;
   mi->Caption="&������� �����";
   mi->OnClick=DeletePointMenuClick;
   mi->Name="DeletePoint";
   mi->OnDrawItem=MenuDrawItem;
   mi->OnMeasureItem=MenuMeasureItem;
   m->Items->Add(mi);
   mi = new TMenuItem(m);
   mi->Tag=-1;
   mi->Caption="-";
   m->Items->Add(mi);
   mi = new TMenuItem(m);
   mi->Tag=-1;
   mi->Caption="�&������� ���������";
   mi->Name="AddMeaasure";
   mi->OnClick=AddMeasureMenuClick;
   mi->OnDrawItem=MenuDrawItem;
   mi->OnMeasureItem=MenuMeasureItem;
   m->Items->Add(mi);
   mi = new TMenuItem(m);
   mi->Tag=-1;
   mi->Caption="�&������ ���������";
   mi->Name="DeleteMeasure";
   mi->OnClick=DeleteMeasureMenuClick;
   mi->OnDrawItem=MenuDrawItem;
   mi->OnMeasureItem=MenuMeasureItem;
   m->Items->Add(mi);
   mi = new TMenuItem(m);
   mi->Tag=-1;
   mi->Caption="-";
   m->Items->Add(mi);
   mi = new TMenuItem(m);
   mi->Tag=-1;
   mi->Caption="�&�������";
   mi->Name="Properties";
   mi->OnClick=PropertiesMenuClick;
//   mi->OnDrawItem=MenuDrawItem;
   mi->OnMeasureItem=MenuMeasureItem;
   mi->Tag=-1;
   m->Items->Add(mi);

   mi = new TMenuItem(m);
   mi->Caption="���������� ��������...";
   mi->Name="SettAll";
   mi->ShortCut=TextToShortCut("F3");
   mi->OnClick=SetAllMenuClick;
//   mi->OnDrawItem=MenuDrawItem;
   mi->OnMeasureItem=MenuMeasureItem;
   mi->Tag=-1;
   SetAllMenuItem=mi;
   m->Items->Add(mi);

   return m;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::AddPointMenuClick(TObject* Sender)
{
   FGridData->AddNew();
   UpdateRowsData();
   AdjustVScrollBars();
   Selection.RowIndex=0;
   Selection.ColumnsIndex=0;
   Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::DeletePointMenuClick(TObject* Sender)
{
   TMICellData Data;
   if(!GetCellData(Data))return;
   if(Data.PointIndex!=-1)
      {
      FGridData->Delete(Data.PointIndex);
      UpdateRowsData();
      AdjustVScrollBars();
      Invalidate();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::AddMeasureMenuClick(TObject* Sender)
{

   TMICellData Data;
   if(!GetCellData(Data)) return ;
   if(Data.PointIndex!=-1)
      {
      FGridData->AddNewMeasure(Data.PointIndex);
      UpdateRowsData();
      AdjustVScrollBars();
      FGridData->Calculate();
      Invalidate();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::PropertiesMenuClick(TObject* Sender)
{
   TMICellData Cell;
   if(!GetCellData(Cell)) return;
   if(Cell.PointIndex==-1) return;

   AnsiString s;
   Cell.vo->FormatStr(Cell.v,s);
   PropertiesForm->ValueLabel->Caption=s;

   if(Cell.vo->Bitmap!=-1)
      {
      PropertiesForm->NameLabel->Visible=false;
      PropertiesForm->NameImage->Picture->Bitmap=FGridData->OnGetBitmap(Cell.vo->Bitmap);
      }
   else
      {
      PropertiesForm->NameLabel->Visible=true;
      PropertiesForm->NameLabel->Caption=Cell.vo->Name;
      PropertiesForm->NameImage->Picture=0;;
      }
   if(Cell.vo->UnitBitmap!=-1)
      {
      PropertiesForm->UnitLabel->Visible=false;
      PropertiesForm->UnitImage->Picture->Bitmap=FGridData->OnGetBitmap(Cell.vo->UnitBitmap);
      }
   else
      {
      PropertiesForm->UnitLabel->Visible=true;
      PropertiesForm->UnitLabel->Caption=Cell.vo->Unit;
      PropertiesForm->UnitImage->Picture=0;;
      }
   PropertiesForm->DescriptionLabel->Caption=Cell.vo->Description;
   PropertiesForm->PointLabel->Caption=Cell.PointIndex+1;
   if(Cell.Type==/*TRowType::*/PointRow)
      PropertiesForm->MeasureLabel->Caption="";
   else
      PropertiesForm->MeasureLabel->Caption=Cell.MeasureIndex+1;
   if(Cell.vo->Kind==TVarOptions::InputData)
      PropertiesForm->KindLabel->Caption="�������� ������";
   else
      PropertiesForm->KindLabel->Caption="����������� ������";
   TColor cl=TVar::Colors[Cell.v->State*2];
   switch(Cell.v->State)
      {
      case TVar::UserInput:
         s="��������� ������������� ������";
         break;
      case TVar::UsedNearest:
         s="������������� ��������";
         break;
      case TVar::ErrorValue:
         s="�������� ��������";
         break;
      case TVar::Calculated:
         s="���������";
         break;
      case TVar::ErrorArg:
         s="������ ��� ����������. �������� �������� ������";
         break;
      case TVar::ErrorResult:
         s="������ ��� ����������. �������� ��������� ����������";
         break;
      case TVar::NotUsed:
         s="�� ������������";
         break;
      }
   PropertiesForm->StateLabel->Caption=s;
   if(Cell.vo->CalcOptions.CalcRoundType==TCalcOptions::None)
      s="��� ���������";
   else
      if(Cell.vo->CalcOptions.CalcRoundType==TCalcOptions::AfterDot)
         {
         s=Cell.vo->CalcOptions.CalcRoundNumber;
         s+=" ������ ����� �������";
         }
      else
         {
         s=Cell.vo->CalcOptions.CalcRoundNumber;
         s+=" �������� ����";
         }
   PropertiesForm->CalcLabel->Caption=s;
   if(Cell.vo->CalcOptions.ShowRoundType==TCalcOptions::None)
      s="��� ���������";
   else
      if(Cell.vo->CalcOptions.ShowRoundType==TCalcOptions::AfterDot)
         {
         s=Cell.vo->CalcOptions.ShowRoundNumber;
         s+=" ������ ����� �������";
         }
      else
         {
         s=Cell.vo->CalcOptions.ShowRoundNumber;
         s+=" �������� ����";
         }
   PropertiesForm->ShowLabel->Caption=s;

   PropertiesForm->StateLabel->Font->Color=cl;

   PropertiesForm->ShowModal();;
//TODO: changed 08.08.2006
/*   if(!FPropertiesEvent) return;
   TMICellData Data;
   if(!GetCellData(Data)) return;
   if(Data.PointIndex!=-1)
      {
      FPropertiesEvent(FGridData,&Data);
      }*/
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::DeleteMeasureMenuClick(TObject* Sender)
{
   TMICellData Data;
   GetCellData(Data);
   if(Data.Type==MeasureRow )
      {
      FGridData->GridPoints[Data.PointIndex]->Delete(Data.MeasureIndex);
      UpdateRowsData();
      AdjustVScrollBars();
      Invalidate();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::AdjustVScrollBars(void)
{
   SCROLLINFO s;
   s.cbSize=sizeof(s);
   s.nMin=0;
   s.nMax=FRowsHeight + Header->Height;;
   s.nPos=0;
   s.nPage=ClientHeight - Header->Height;
   s.fMask=SIF_RANGE|SIF_POS|SIF_PAGE;
   SetScrollInfo(Handle,SB_VERT ,&s,true);
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMVScroll(Messages::TWMScroll & Msg)
{
   int top=FTopRow;
   Msg.Result=0;
   if(!FGridData) return;
   int PointCount=FGridData->Count;
   int MeasureCount;
   if(PointCount==0) return;
//   TGridPoint* p;
   switch (Msg.ScrollCode)
   {
      case SB_LINEUP:
         TopRow--;
         break;
      case SB_LINEDOWN:
         TopRow++;
         break;
      case SB_PAGEUP:
         while( (top>0) && (LastVisibleRow(top)!=FTopRow) )
            top--;
         TopRow=top;
         break;
      case SB_PAGEDOWN:
         TopRow=LastVisibleRow();
         break;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::UpdateRowsData()
{
   if(!FGridData) return;
   //Clear rows list;
   int c=FRows->Count;
   for(int i=0;i<c;i++)
      delete (TMIRow*)FRows->Items[i];
   FRows->Clear();
   int PointCount=FGridData->Count;
   int MeasureCount;
   TGridPoint* p;
   TMIRow* row;
   int y=0;
   //iterate through points
   for(int i=0;i<PointCount;i++)
      {
      p = FGridData->GridPoints[i];
      MeasureCount=p->Count;
      //Add measure rows
      for(int j=0;j<MeasureCount;j++)
         {
         row = new TMIRow();
         row->PointIndex=i;
         row->Type=MeasureRow;
         row->RowIndex=j;
         row->YOffset=y;
         FRows->Add(row);
         y+=FRowHeight;
         }
      //Add point rows
      for(int pi=0;pi<FPointRows.Rows;pi++)
         {
         row = new TMIRow();
         row->PointIndex=i;
         row->Type=PointRow;
         row->RowIndex=pi;
         row->YOffset=y;
         FRows->Add(row);
         y+=FPDO->Height;
         }
      }
   FRowsHeight=y;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::DrawRow(int RowIndex)
{
   if((!FGridData)||(RowIndex<FTopRow)) return;
   if((RowIndex<0)||(RowIndex>=FRows->Count)) return;
   TMIRow* TopRow;
   TMIRow* DrawRow;
   TopRow = (TMIRow*) FRows->Items[FTopRow];
   DrawRow = (TMIRow*) FRows->Items[RowIndex];
   TGridPoint* DrawPoint=FGridData->GridPoints[DrawRow->PointIndex];
   int y=Header->Height+FOffsetFromHeader+DrawRow->YOffset-TopRow->YOffset;
   if(y>ClientHeight) return;
   if(DrawRow->Type==MeasureRow)
      {
      DrawMeasureRow(DrawPoint->Measures[DrawRow->RowIndex],y,
                  (RowIndex==Selection.RowIndex));
      }
   else
      DrawPointRow(RowIndex,y);

}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::DrawPointRow(int RowIndex, int y)
{
   TMIRow* row = (TMIRow*) FRows->Items[RowIndex];
   int LeftCell,CountCell;
   LeftCell=row->RowIndex*FPointRows.RowCells;
   CountCell=FPointRows.RowCells;
   int k=FGridData->PointColumns.Count;
   int x=0;
   for(int i=0;i<CountCell;i++)
      {
      if((i+LeftCell)>=k) break;
      x+=DrawPointCell(row->PointIndex,LeftCell+i,x,y,
         ((RowIndex==Selection.RowIndex)&&(i==Selection.ColumnsIndex)));
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::SetTopRow(int value)
{
   if( (value<0)||((value)>=FRows->Count) )
      return;
   if(FTopRow != value)
      {
      FTopRow = value;
      SCROLLINFO s;
      s.cbSize=sizeof(s);
      TMIRow* Row=(TMIRow*) FRows->Items[value];
      s.nPos=Row->YOffset;
      s.fMask=SIF_POS;
      SetScrollInfo(Handle,SB_VERT ,&s,true);
      Invalidate();
      }
}
//---------------------------------------------------------------------------
int __fastcall TMIGrid::LastVisibleRow(int topRow)
{
   int RowCount=FRows->Count;
   if((!FGridData)||(RowCount==0))
      return 0;
   if(topRow==-1)
      topRow=FTopRow;
   if((topRow<0)||(topRow>=RowCount))
      return 0;
   TMIRow* row1 = (TMIRow*) FRows->Items[topRow];
   int y=Header->Height+FOffsetFromHeader-row1->YOffset;
   int ch=ClientHeight;
   int row=FTopRow;
   for(;row<RowCount;row++)
      {
      TMIRow* itrow = (TMIRow*) FRows->Items[row];
      int w;
      if(itrow->Type==PointRow)
         w=FPDO->Height;
      else
         w=FRowHeight;
      if( (y+itrow->YOffset+w)>ch)
         {
         row--;
         if(row>=0) return row;
            else return 0;
         }
      }
   return RowCount;
}
//---------------------------------------------------------------------------
bool __fastcall TMIGrid::GetCellData(TMICellData& Data, TMIGridCoord* Coord)
{
   if(Coord==NULL)
      Coord=&Selection;
   if((Coord->RowIndex<0)||(Coord->ColumnsIndex<0))
      return false;
   if(FGridData->Count==0)
      return false;
   TMIRow* row= (TMIRow*) FRows->Items[Coord->RowIndex];
   Data.Type=row->Type;
   Data.PointIndex=row->PointIndex;
   TGridPoint* p=FGridData->GridPoints[row->PointIndex];
   if(row->Type==MeasureRow)
      {
      Data.VarIndex=FGridData->MeasureColumns.Values[Coord->ColumnsIndex];
      Data.vo=FGridData->MeasureVarOptions[Data.VarIndex];
      Data.MeasureIndex=row->RowIndex;
      Data.v=p->Measures[Data.MeasureIndex]->Var[Data.VarIndex];
      }
   else
      {
      Data.VarIndex=FGridData->PointColumns.Values[row->RowIndex*FPointRows.RowCells+
                                                Coord->ColumnsIndex];
      Data.vo=FGridData->PointVarOptions[Data.VarIndex];
      Data.v=p->Var[Data.VarIndex];
      }
   return true;

}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::OnDrawHeaderSection(THeaderControl* HeaderControl,
         THeaderSection* Section, const Types::TRect &Rect, bool Pressed)
{
   if(!FGridData->OnGetBitmap) return;
   TVarOptions* vo=FGridData->MeasureVarOptions[GridData->MeasureColumns.Values[Section->Index]];
   HeaderControl->Canvas->CopyMode=cmSrcAnd		;
   Graphics::TBitmap* b=FGridData->OnGetBitmap(vo->Bitmap);
   if(b)
      {
      int w=b->Width;
      int h=b->Height;
      HeaderControl->Canvas->Draw(Rect.Left+(Rect.Width()-w)/2,Rect.Top+(Rect.Height()-h)/2,b);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::SetHeaderHeight(int value)
{
   if(Header->Height != value) {
      Header->Height = value;
   }
}
//---------------------------------------------------------------------------
int __fastcall TMIGrid::GetHeaderHeight()
{
   return Header->Height;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::OnChangeItemsStructure(TObject* o)
{
	TWMSize sz;
	WMSize(sz);
	UpdateRowsData();
   Setup();
   TGridPoint* p;
   int pi;
   bool find=false;
   int PointCount=FGridData->Count;
   //iterate through points
   for(int i=0;i<PointCount;i++)
      {
      p = FGridData->GridPoints[i];
      if(p==SelectedPoint)
         {
         find=true;
         pi=i;
         break;
         }
      }
   if(find)
      {
      TMIRow* row;
      Selection.RowIndex=0;
      int c=FRows->Count;
      for(int i=0;i<c;i++)
         {
         row = (TMIRow*)FRows->Items[i];
         if( (row->PointIndex==pi) && (row->RowIndex==SelectedRow.RowIndex) &&
                  (row->Type==SelectedRow.Type))
            {
            SelectedRow=*row;
            break;
            }
         Selection.RowIndex++;
         }
      }
   else
      {
      SelectedPoint=0;
//      SelectedRow=0;
      Selection.RowIndex=-1;
      Selection.ColumnsIndex=-1;
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::OnSectionDragEvent(System::TObject* Sender, THeaderSection* From,
                                    THeaderSection* To, bool &AllowDrag)
{
   FromSection=From->Index;
   ToSection=To->Index;
   AllowDrag=true;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::OnDragEnd(TObject* Sender)
{
   GridData->MeasureColumns.Move(FromSection,ToSection);
   Invalidate();
   Repaint();
   Update();

   Header->Sections->Clear();
   int c=FGridData->MeasureColumns.Count;
   for(int i=0;i<c;i++)
      {//Add section to header
      Header->Sections->Add();
      Header->Sections->Items[i]->Alignment=taCenter;
      TVarOptions* vo=FGridData->MeasureVarOptions[GridData->MeasureColumns.Values[i]];
      Header->Sections->Items[i]->Text=vo->Name;
      Header->Sections->Items[i]->Width=vo->Width;

      if(vo->Bitmap!=-1)
         Header->Sections->Items[i]->Style=hsOwnerDraw;
      }
   AdjustHScrollBars();
   AdjustVScrollBars();
   }
//---------------------------------------------------------------------------
void __fastcall TMIGrid::DoContextPopup(const Types::TPoint &MousePos, bool &Handled)
{

   TPoint Mouse=MousePos ;
   if( ((Mouse.x==-1) && (Mouse.y==-1)) || (FRows->Count==0))
      {//Set coordinates for
      TRect r;
      if(GetCellRect(r,&Selection))
         {
         Mouse.x=r.Left;
         Mouse.y=r.Bottom;
         }
      else
         {
         if((Mouse.x==-1) && (Mouse.y==-1))
            {
            Mouse.x=4;
            Mouse.y=GetHeaderHeight()+4;
            }
         }
      Mouse=ClientToScreen(Mouse);
      FGridPopupMenu->Popup(Mouse.x,Mouse.y);
      Handled=true;
      }
   TCustomControl::DoContextPopup(Mouse,Handled);
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MenuDrawItem(TObject *Sender, TCanvas *ACanvas,
      const TRect &ARect, bool Selected)
{
   TMenuItem* i = (TMenuItem*) Sender;
   int mi=i->MenuIndex;

   if(Selected	)
      ACanvas->Brush->Color=clMenuHighlight;//clWhite;
   else
      ACanvas->Brush->Color=clMenu;
   ACanvas->FillRect(ARect);

   if(mi <=1)
      ACanvas->Font->Style=TFontStyles()<< fsBold;
   else
      ACanvas->Font->Style = TFontStyles();

   ACanvas->Font->Color=clBlack;
   if( (mi==0) || (mi==3))
      ACanvas->Font->Color=clBlue;
   if( (mi==1) || (mi==4))
      ACanvas->Font->Color=clRed;

   int y= ACanvas->TextHeight(i->Caption);
   tagRECT r;
   r.left=ARect.left+4;
   r.right=ARect.right;
   r.top=ARect.bottom - 2 - y;
   r.bottom=ARect.bottom;
   DrawText(ACanvas->Handle,i->Caption.c_str(),i->Caption.Length(),&r, 0);
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::MenuMeasureItem(TObject *Sender, TCanvas *ACanvas,
      int &Width, int &Height)
{
   TMenuItem* i = (TMenuItem*) Sender;
   TSize sz=Canvas->TextExtent(i->Caption);
   Width=6+sz.cx;
   Height=4+sz.cy;
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::WMMouseWheel(TMessage &Msg)
{
   if( ((short) HIWORD(Msg.WParam)) <0)
      {
      if(Msg.WParamLo==MK_SHIFT)
         MoveRight();
      else
         MoveDown();
      }
   else
      {
      if(Msg.WParamLo==MK_SHIFT)
         MoveLeft();
      else
         MoveUp();
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIGrid::SetAllMenuClick(TObject* Sender)
{
//         FGridData->SetValue(val,&Data);
   TMICellData Cell;
   if(!GetCellData(Cell)) return;
   if(Cell.PointIndex==-1) return;
   if(Cell.Type!=MeasureRow) return;

   TGridPoint* p1 =  FGridData->GridPoints[Cell.PointIndex];
   //Calcuate PointNumber
   int PointNumber=0;
   int i=0;
   do
      {
      TGridPoint* p2=FGridData->UsedPoint[i];
      if(p2==NULL) break;
      if(p1==p2)
         {
         PointNumber=i+1;
         break;
         }
      i++;
      }
   while(1);
   SetAllForm->Label5->Caption=PointNumber;
   //p1->Var[TMeasurePoint::Number]->Value.i;
   SetAllForm->Label6->Caption=Cell.vo->Description;
   if(Cell.vo->Type==TVarOptions::Integer)
      {
//#pragma warn -8018
      SetAllForm->NumberEdit1->NumberType=TNumberEdit::Integer;
      SetAllForm->NumberEdit1->Text=Cell.v->Value.i;
      }
   else
      {
      SetAllForm->NumberEdit1->NumberType=TNumberEdit::Double;
      SetAllForm->NumberEdit1->Text=Cell.v->Value.d;
      }
   if(SetAllForm->ShowModal()==mrOk)
      {
      //Set values to all cell's
      int c=p1->Count;
      TVar::TValue v;
      try
         {
         if(Cell.vo->Type==TVarOptions::Integer)
            v.i=SetAllForm->NumberEdit1->Text.ToInt();
         else
            v.d=SetAllForm->NumberEdit1->Text.ToDouble();
         }
      catch(EConvertError&)
         {
         return;
         }
      for(int i=0;i<c;i++)
         {
         TVarContainer* m = (p1->Measures[i]);
         m->Var[Cell.VarIndex]->Value=v;
         Cell.vo->ProcessRound(m->Var[Cell.VarIndex]);
         m->Var[Cell.VarIndex]->State=TVar::UsedNearest;
         }
      FGridData->Calculate();
      }
//#pragma warn +8018
}
//---------------------------------------------------------------------------
