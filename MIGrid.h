//---------------------------------------------------------------------------

#ifndef MIGridH
#define MIGridH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ComCtrls.hpp>

#include "PropertiesForm_unit.h"
#include "SetAllForm_unit.h"
#include "Edit_unit.h"
#include "variable.h"


//---------------------------------------------------------------------------
struct TMIGridCoord
{
   int RowIndex;
   int ColumnsIndex;
};
//---------------------------------------------------------------------------
struct TMIRow
{
   int PointIndex;
   int RowIndex;
   TRowType Type;
   int YOffset;
};
//---------------------------------------------------------------------------
class TMIPointDrawOptions: public TPersistent
{
private:
   int FLineWidth;
   int FCaptionWidth;
   int FValueWidth;
   int FSelectedLineWidth;
   TColor FCaptionColor;
   int FHeight;
   int FCellsSpace;
__published:
   __property int LineWidth  = { read=FLineWidth, write=FLineWidth, default=1 };
   __property int CaptionWidth  = { read=FCaptionWidth, write=FCaptionWidth, default=40 };
   __property int ValueWidth  = { read=FValueWidth, write=FValueWidth, default=48 };
   __property int SelectedLineWidth  = { read=FSelectedLineWidth, write=FSelectedLineWidth, default=2 };
   __property TColor CaptionColor = {read=FCaptionColor, write=FCaptionColor, default = clMoneyGreen};
   __property int Height  = { read=FHeight, write=FHeight, default =24 };
   __property int CellsSpace  = { read=FCellsSpace, write=FCellsSpace, default=4 };
public:
   __fastcall TMIPointDrawOptions():TPersistent(){FLineWidth=1;FCaptionWidth=40;
                     FValueWidth=48; FSelectedLineWidth=2; FCaptionColor=clMoneyGreen;
                     FHeight=24;FCellsSpace=4;}
};
//---------------------------------------------------------------------------
class PACKAGE TMIGrid : public TCustomControl
{
private:

   TPropertiesForm *PropertiesForm;
   TSetAllForm *SetAllForm;

   int ToSection;
   int FromSection;
   TList* FRows;
   int FRowsHeight;
   TPopupMenu* FGridPopupMenu;
   TMenuItem* SetAllMenuItem;
   void __fastcall OnResizeHeader(THeaderControl* HeaderControl, THeaderSection* Section);
   TColor FColor;
//   TFont* FFont;
   void __fastcall SetColor(TColor value);
   Graphics::TBitmap* Check[3];

   TGridData* FGridData;
   TMIGridCoord EditedCell;
   int FRowHeight;
   int FLeftCol;
   int FOffsetFromHeader;
   struct TPointRows
      {
      int Rows;
      int RowCells;
      int LastRowCells;
      } FPointRows;
   bool FEditorMode;
   TMIEdit* FEditor;
   TMIComboEdit* FComboEditor;

   TMIPointDrawOptions* FPDO;
   int FTopRow;
//   void __fastcall SetFont(TFont* value);
   void __fastcall SetRowHeight(int value);
   void __fastcall SetGridData(TGridData* GridData);
   void __fastcall SetTopRow(int value);
   void __fastcall SetHeaderHeight(int value);
   int __fastcall GetHeaderHeight();

protected:
   THeaderControl* Header;
   DYNAMIC void __fastcall Click(void);
   virtual void __fastcall Paint(void);
   DYNAMIC bool __fastcall CanFocus(void);
   DYNAMIC void __fastcall KeyPress(char &Key);
   DYNAMIC void __fastcall DoContextPopup(const Types::TPoint &MousePos, bool &Handled);
   virtual void __fastcall CreateParams(TCreateParams &Params);
	void __fastcall WMSetFocus(TMessage &Msg);
	void __fastcall WMKillFocus(TWMKillFocus &Msg);
	void __fastcall WMMouseWheel(TMessage &Msg);

BEGIN_MESSAGE_MAP
  MESSAGE_HANDLER(WM_MOUSEWHEEL, TMessage, WMMouseWheel)
  MESSAGE_HANDLER(WM_KILLFOCUS, TWMKillFocus, WMKillFocus)
  MESSAGE_HANDLER(WM_SETFOCUS, TMessage, WMSetFocus)
      VCL_MESSAGE_HANDLER(WM_VSCROLL, Messages::TWMScroll, WMVScroll)
      VCL_MESSAGE_HANDLER(WM_HSCROLL, Messages::TWMScroll, WMHScroll)
      VCL_MESSAGE_HANDLER(WM_GETDLGCODE, Messages::TWMNoParams, WMGetDlgCode)
      VCL_MESSAGE_HANDLER(WM_SIZE, TWMSize, WMSize)

      VCL_MESSAGE_HANDLER(WM_LBUTTONDBLCLK, TWMLButtonDblClk, WMLButtonDblClick)
END_MESSAGE_MAP(TControl)

   DYNAMIC void __fastcall MouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
   DYNAMIC void __fastcall KeyDown(Word &Key, Classes::TShiftState Shift);
   void __fastcall WMGetDlgCode(Messages::TWMNoParams & Msg);
   int __fastcall LastFullVisibleCol(int LeftCol=-1);
   int __fastcall DrawPointCell(int PointIndex, int ColumnIndex, int x, int y,bool Sel=false);
   TMIEdit* __fastcall CreateEditor(void);
   TMIComboEdit* __fastcall CreateComboEditor(void);
   bool __fastcall GetEditRect(TRect& Rect, TMIGridCoord* Selection);
   void __fastcall WMLButtonDblClick(TWMLButtonDblClk& Msg);

private:
   void __fastcall MoveRight();
   void __fastcall MoveLeft();
   void __fastcall MoveDown();
   void __fastcall MoveUp();
   DYNAMIC ::Menus::TPopupMenu* __fastcall GetPopupMenu(void);
   TPopupMenu* __fastcall CreateGridPopupMenu();
   void __fastcall AddPointMenuClick(TObject* Sender);
   void __fastcall DeletePointMenuClick(TObject* Sender);
   void __fastcall AddMeasureMenuClick(TObject* Sender);
   void __fastcall DeleteMeasureMenuClick(TObject* Sender);
   void __fastcall PropertiesMenuClick(TObject* Sender);
   void __fastcall UpdateRowsData();
   void __fastcall DrawRow(int RowIndex);
   void __fastcall DrawPointRow(int RowIndex, int y);

   void __fastcall OnDrawHeaderSection(THeaderControl* HeaderControl, THeaderSection* Section,
                                       const Types::TRect &Rect, bool Pressed);

   void __fastcall MenuDrawItem(TObject *Sender, TCanvas *ACanvas,
                                                  const TRect &ARect, bool Selected);
   void __fastcall MenuMeasureItem(TObject *Sender, TCanvas *ACanvas,
                                                      int &Width, int &Height);

   void __fastcall OnChangeItemsStructure(TObject* f);

public:
   __fastcall TMIGrid(TComponent* Owner);
   __fastcall ~TMIGrid();

   __property TGridData* GridData  = { read=FGridData, write=SetGridData };

   bool __fastcall GetCellData(TMICellData& Data, TMIGridCoord* Coord = NULL);
   void __fastcall SetAllMenuClick(TObject* Sender);
private:
   TMIGridCoord Selection;
   TMIRow SelectedRow;
   TGridPoint* SelectedPoint;
   bool __fastcall GetCellRect(TRect& Rect, TMIGridCoord* Selection);
   void __fastcall Setup();
   void __fastcall DrawMeasureRow(TVarContainer* Measure, int y,bool Selected=false);
   void __fastcall DrawCell(TCanvas* Canvas,TRect Rect,TVar* v,TVarOptions* vo,bool Selected=false);
   void __fastcall WMVScroll(Messages::TWMScroll & Msg);
   void __fastcall WMHScroll(Messages::TWMScroll & Msg);
   void __fastcall AdjustHScrollBars(void);
   void __fastcall AdjustVScrollBars(void);
   void __fastcall SetLeftCol(int LeftCol);

   void __fastcall WMSize(TWMSize& Msg);
   void __fastcall Edit(TMIGridCoord* s=NULL,char Key=0);
   void __fastcall OnHideEditor(System::TObject* Sender,bool &Changed);
   void __fastcall MouseToCell(int x, int y, TMIGridCoord& Cell);
   int __fastcall LastVisibleRow(int TopRow=-1);

   __property int LeftCol  = { read=FLeftCol,write=SetLeftCol,default=0 };
   __property int TopRow  = { read=FTopRow, write=SetTopRow };
//   __property TMIGridRow UpRow  = { read=FUpRow, write=SetUpRow };
   void __fastcall OnSectionDragEvent(System::TObject* Sender, THeaderSection* FromSection,
                                    THeaderSection* ToSection, bool &AllowDrag);
   void __fastcall OnDragEnd(TObject* Sender);
__published:
   __property TColor Color  = { read=FColor, write=SetColor,default=clInfoBk };
//   __property TPropertiesEvent OnProperty= {read=FPropertiesEvent, write=FPropertiesEvent};
//   __property TFont* Font  = { read=FFont, write=SetFont };
   __property Font;
   __property int RowHeight  = { read=FRowHeight, write=SetRowHeight, default=18 };
   __property TMIPointDrawOptions* PointDrawOptions  = { read=FPDO, write=FPDO };
   __property Align;
   __property int HeaderHeight  = { read=GetHeaderHeight, write=SetHeaderHeight, default=24 };
};
//---------------------------------------------------------------------------
#endif
