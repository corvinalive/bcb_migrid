//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SetAllForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TSetAllForm *SetAllForm;
//---------------------------------------------------------------------------
__fastcall TSetAllForm::TSetAllForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSetAllForm::FormShow(TObject *Sender)
{
   ActiveControl=NumberEdit1;
   NumberEdit1->SelectAll();
}
//---------------------------------------------------------------------------
void __fastcall TSetAllForm::NumberEdit1KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if(Key==VK_RETURN)   BitBtn1->Click();
}
//---------------------------------------------------------------------------

