//---------------------------------------------------------------------------

#ifndef SetAllForm_unitH
#define SetAllForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include "NumberEdit.h"
//---------------------------------------------------------------------------
class TSetAllForm : public TForm
{
__published:	// IDE-managed Components
   TLabel *Label1;
   TLabel *Label2;
   TLabel *Label3;
   TLabel *Label4;
   TBitBtn *BitBtn1;
   TBitBtn *BitBtn2;
   TLabel *Label5;
   TLabel *Label6;
   TNumberEdit *NumberEdit1;
   void __fastcall FormShow(TObject *Sender);
   void __fastcall NumberEdit1KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
   __fastcall TSetAllForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
#endif
